# Prevention of Sexual Harassment

## 1. What kinds of behaviour cause sexual harassment?

- Sexual harassment includes unwelcome sexual advances, requests for sexual favors, and other verbal or physical harassment of a sexual nature in the workplace or learning environment, according to the Equal Employment Opportunity Commission (EEOC). Sexual harassment does not always have to be specifically about sexual behavior or directed at a specific person. For example, negative comments about women as a group may be a form of sexual harassment.

- There are generally three forms of  sexual harassment behavior verbal, visual, and physical.
- Verbal harassment might include someone making sexually suggestive comments to one person, for example remarking on your body or appearance, or name-calling.
If the person is subjected to sexual jokes that make him feel uncomfortable, offended or intimidated, this counts as sexual harassment.
- At first glance “visual harassment” by definition may seem obvious in that one individual is exposing themselves to another individual who does not appreciate the exposure. However, visual harassment comes in other forms that are not as blatant as perhaps a fellow employee exposing themselves. Visual harassment can be demonstrated through cartoons or drawings considered offensive and/ or insulting to the victim. 
- Physical sexual harassment is the most obvious and well-known form of sexual harassment. It is exercised through unwelcome touching such as rubbing up against a person or physically interfering with another’s movements or preventing another from completing their work. Examples of unwanted touching would be if employee A placed his arms around employee B and employee B felt uncomfortable with this and asked employee A to stop.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- Research your company's policy -  
If you're being bullied at work or know of someone who is, research your company's policies to determine the proper protocols to report such activity.

- Talk to your higher-ups or HR - 
If you are not comfortable speaking to the individual who is bullying you directly, you might need to discuss it with your manager or human resources.

- Address the issue directly and rise above - 
This won't always be possible or comfortable, but it is often best to speak up and stand your ground when communicating with a bully.

- Document all of it - 
This bit of advice on how to handle bullying in the workplace is extremely important to remember: Always document everything related to your interactions with the bully. This not only provides a timeline of events, but it also helps you recall information more easily when needed. 

- If you don't get a resolution at work after going through all of the channels you know to go through, you might want to speak to an attorney to discuss your options. 

- Sexual Harassment of Women at Workplace (Prevention, Prohibition and Redressal) Act, 2013 - 
In 2013, the Government of India notified the Sexual Harassment of Women at Workplace (Prevention, Prohibition, and Redressal) Act. Consistent with the Vishaka judgment, the Act aspires to ensure women’s right to workplace equality, free from sexual harassment through compliance with the elements of Prohibition, Prevention, and Redressal.


- Laws against Cyber Bullying - 
The Indian Penal Code, 1860 (“IPC”), neither defines bullying nor punishes it as an offence. However, various provisions of the IPC and the Information Technology Act, 2000 (“IT Act”) can be used to fight cyber bullies.
