# How to Learn Faster with the Feynman Technique

1. What is the Feynman Technique? Paraphrase the video in your own words?

Feynman technique is a way of learning better which helps anyone facing a situation 
- who has to study for an exam 
- a business professional who has to understand a topic
- a baker who has to remember the recipe for a cheesecake
the Feynman technique is one of the useful self learning techniques for every single person.
The process helps us to solve any of the  common problems faced during learning:
- Not able to understand the concept well enough
- Forgetting what you learned
- Failing to learn effectively

2. What are the different ways to implement this technique in your learning process?

There are four steps to implement this technique:

- choose a concept to learn and write down the notes 
- Explain the concept to someone who is not familiar  with the topic , Explainit in a simple formate
- Identify the mistakes and fill the knowledge gaps by from the notes 
- simplify the explination and create analogies

3. Paraphrase the video in detail in your own words.

The Pomodoro Technique is a time‐management tool and the popular icon associated with the Pomodoro Technique is the tomato‐shaped kitchen
timer An essential component of the Pomodoro Technique is that it requires the student to actively plan out each study session and be thoughtful about what tasks to accomplish. Another essential component of the Pomodoro Technique is that it teaches users how to effectively take breaks when working.

Right now, we are probably have readings to catch up on, a paper to write, and that nagging physics homework in our backpack.
The Pomodoro Technique helps us plan out our course of action in a very thoughtful way so we feel we have a sense of
direction. Sometimes “time” is the worst enemy; if we have too much time before the work must be done.
If we don’t have enough time, we will rush through it and end up with a product that doesn’t really represent what we are capable of. The Pomodoro Technique will help us because it will teach us to use time as a tool to systematically accomplish each task on our to‐do list. Also, when we work, we often reward ourself with breaks. The Pomodoro Technique will help us to structure our time by taking small, frequent breaks.
The Pomodoro Technique also helps us learn to accurately.


4. What are some of the steps that you can take to improve your learning process?

- pick a task
- Estimate the time requires to complete
- set timer to 25
- work intansly without any distractions
- Take 5 mins break  once 25 mins are up
- after 4 pomodors take a long 30 mins break 

5. Your key takeaways from the video? Paraphrase your understanding.

Suppose if you want to learn a new language, want to learn how to draw then If you put 20 hours of focused deliberate practice in that skill that you think of, you will be outstanding in that work.

Four simple steps for rapid skill aquisition:

- Deconstruct the skill: Decide exactly what you want to do when you're done and look into the skill break it down into smaller pieces.

- Learn enough to self-correct: Get the three or five resources about what it is trying to learn could be book, could be course and practice.

- Remove practice barriers: distractions like Telivision, Internet and mobile phones.

- Practice atleast 20 hours: Just 45 mins a day you will complete or learn the skill within a month.

6. What are some of the steps that you can while approaching a new topic?

According to me,

Focus on one skill at a time:

Generally it takes 3–6 months to good at some skills. But it takes determination, Passion and dedication which makes you perfect.
Everyone is having their on speed to learn, so might one can learn hone his skills in few days and other will take months.
It is my personal experience. One must learn things early morning. You will grasp better.
Try to continue for minimum 21 days. So you'll get persistency.
There is famous quote I would like to mention if egg is break from outside everything will be destroyed & if it's from inside wonders will be happened.

