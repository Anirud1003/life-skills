# Representational State Transfer (REST) services
Representational State Transfer (REST) is a lightweight framework for designing applications that use HTTP to make calls. REST uses HTTP to perform the Create, Read, Update, and Delete (CRUD) operations between client and server. Applications interact with the services by using HTTP, POST, PUT, GET, and DELETE operations.

HCL Commerce uses Representational State Transfer (REST) services to provide a framework that can be used to develop RESTful applications on several platforms. These platforms can include web, mobile, kiosks, and social applications.

Comparatively, REST can perform the same functions as the Simple Object Access Protocol (SOAP) and Web Services Description Language (WSDL).

REST is platform and language independent, and because it is built on HTTP, is compatible in environments behind firewalls. REST can use secure connections over HTTPS to take advantage of HTTPS security features such as encryption or username and password tokens.

REST calls include all the information that is required to return results (state transfer), eliminating the need for cookies when you are using REST services in the storefront.

REST services help facilitate the invocation of classic controller commands and the activation of data beans. They aim to provide a framework that is easy to learn and customize. The framework lets you create custom REST resource handlers that invoke controller commands to perform add, update and delete operations, or activate data beans to retrieve data.

## REST services characteristics

The following list summarizes the key characteristics of the REST architecture:
Uses a client-server system.
Stateless.
Supports caching of resources.
Proxy servers are supported.
Uses logical URLs to identify resources.

##  REST services abstraction

The built-in Context Providers search for certain elements in the request such as store ID, language ID, or user identity. The Context Providers use these elements to build the appropriate context that is used to retrieve or update the Business Object Document (BOD) in HCL Commerce. The primary context providers are:
Business context provider
Security context provider
These context providers are invoked for every request and provide context as appropriate to the resource handlers.
The Resource Handlers represent the entry points for resource requests and are annotated with the Path, context and any other information that they might require to handle a request. The resource handlers are responsible for coordinating the BOD request and response and converting the request and response to and from a form that is consumable by the client by using standard HTTP protocol elements. They are also responsible for composite resource representations where more than one BOD or source are integrated. In addition, the resource handlers are also responsible for ensuring that related resources are correctly identified and specified in the representation.
Each resource handler implements com.ibm.commerce.foundation.rest.resourcehandler.IResourceHandler interface. You can customize a resource handler by overriding its methods.

The Helpers aid the resource handlers to bridge the BOD layer and enables reusable common code across handlers. There are also helpers for store configuration, building URI, and for enforcing transport security requirements that are specified for resource handlers.
Data Mappers are configuration files that are used to transform resource representations to and from BOD attributes. This enables you to declaratively customize the representation.
The default Entity Providers enable standard encoding of responses as JSON or XML format that is based on data mappers. You can add your own custom providers for other media types to suit your business needs.
Resource Templates are a mechanism to enable you to render custom representations such as XHTML using a JSP file.

## REST services in HCL Commerce

RESTful applications can be developed on several platforms for HCL Commerce. The following diagram illustrates the various REST client platforms and servers:

Mobile Applications

REST services enable the development of mobile applications that leverage device platform-specific native user interfaces, or an embedded web browser for the user experience and REST services for data and updates.
Web Applications

Web applications can include traditional storefronts, or specific Web functionality that provide HCL Commerce functionality through REST services. The amount of web client and server interactions can vary with the REST services that characterize such applications.
On-premise Web applications denote that the application (or part of the application) runs on the HCL Commerce server.
Social applications

These applications are rendered within social containers such as Facebook. Social applications can extend shopping and customer experiences.
On-premise social applications denote that the application (or part of the application) runs on the HCL Commerce server.
Kiosk/desktop applications
These applications leverage HCL Commerce services to bridge store location shoppers with the online store and services.

## Basic REST constraints include:

Client and Server: The client and server are separated from REST operations through a uniform interface, which improves client code portability.
Stateless: Each client request must contain all required data for request processing without storing client context on the server.
Cacheable: Responses (such as Web pages) can be cached on a client computer to speed up Web Browsing. Responses are defined as cacheable or not cacheable to prevent clients from reusing stale or inappropriate data when responding to further requests.
Layered System: Enables clients to connect to the end server through an intermediate layer for improved scalability.

## Characteristics of a REST based network

Client-Server: a pull-based interaction style(Client request data from servers as and when needed). 
Stateless: each request from client to server must contain all the information necessary to understand the request, and cannot take advantage of any stored context on the server. 
Cache: to improve network efficiency, responses must be capable of being labeled as cacheable or non-cacheable. 
Uniform interface: all resources are accessed with a generic interface (e.g., HTTP GET, POST, PUT, DELETE). 
Named resources - the system is comprised of resources which are named using a URL. 
Interconnected resource representations - the representations of the resources are interconnected using URLs, thereby enabling a client to progress from one state to another. 

##Principles of REST web service design

Identify all the conceptual entities that we wish to expose as services. (Examples we saw include resources such as : parts list, detailed part data, purchase order)

 Create a URL to each resource. 

Categorize our resources according to whether clients can just receive a representation of the resource (using an HTTP GET), or whether clients can modify (add to) the resource using HTTP POST, PUT, and/or DELETE).

 All resources accessible via HTTP GET should be side-effect free. That is, the resource should just return a representation of the resource. Invoking the resource should not result in modifying the resource.

Put hyperlinks within resource representations to enable clients to drill down for more information, and/or to obtain related information.

 Design to reveal data gradually. Don't reveal everything in a single response document. Provide hyperlinks to obtain more details.

 Specify the format of response data using a schema (DTD, W3C Schema, RelaxNG, or Schematron). For those services that require a POST or PUT to it, also provide a schema to specify the format of the response.

## What changes in a cloud-native environment?

From a developer’s perspective, nothing really changes in choosing between REST or SOAP, but designing your service in a cloud-native environment brings platform perspective into considerations. Service availability and response time plays a critical role in designing enterprise services and cloud native applications. From a security standpoint, WS-Security (Web Service Security) protocol, which provides end-to-end message level security using SOAP messages, is widely applied in cloud computing to protect the security of most cloud computing related web services. But WS-Security uses SAOP header elements to carry security-related information. A SOAP message is of XML-type format and is normally much bigger in size than the actual message in a binary format. This increases the time and processing to communicate and process the data. This can be argument of debate for choosing REST versus SOAP, but there is shift from SOAP to REST irrespective of the platform your application is going to run on.

Late in 2016, Microsoft Azure added SOAP passthrough support to Azure API Management that helps developers to create a proxy for their SOAP APIs in the same way they create proxy for REST/HTTP APIs. Using SOAP passthrough support, you can import WSDL documents and create a new API proxy; the process looks at all SOAP actions in the document and effectively creates those into API endpoints. In a future version, we might see a feature requested to create REST front end using a SOAP back end.

Inside AWS world, most of the AWS APIs are accessible only via REST and have limited support for SOAP. EC2 resources are available via REST or Query API, while SOAP API for EC2 has been deprecated since late 2015. Services such as Amazon S3 and RDS also supports REST while SOAP is only supported via HTTPS; SOAP for HTTP is deprecated. Amazon SQS doesn’t support SOAP anymore. While REST seems to lead AWS APIs, the Amazon API Gateway integrates with the AWS ecosystem and provide support creating, managing and deploying a RESTful API to expose back-end HTTP/HTTPS endpoints, AWS Lambda functions, and/or other AWS services. API Gateway also helps Invoking exposed API methods through the front-end HTTP endpoints.

More and more support leans towards RESTful APIs. Its simplicity with verb-like operations makes it developer-friendly. It’s compatible with most formats and is easy to use. There is no sunset for SOAP either, but REST is definitely going to be popular among the developer community.

Reference: https://www.infoworld.com/ ,
https://help.hcltechsw.com/.





